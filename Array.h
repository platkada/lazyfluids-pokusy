#ifndef LAZYFLUIDS_POKUSY_ARRAY_H
#define LAZYFLUIDS_POKUSY_ARRAY_H

#include <cstddef>
#include <type_traits>
#include <cassert>
#include <utility>
#include <cmath>
#include <functional>

#define STBI_ONLY_JPEG
#define STBI_ONLY_PNG
#define STB_IMAGE_IMPLEMENTATION

#include "stb_image.h"

#define STB_IMAGE_WRITE_IMPLEMENTATION

#include "stb_image_write.h"

#include "util.h"

template<class T>
struct traits;

template<class T> struct traits<const T> : traits<T> {};
template<class T> struct traits<const T &> : traits<T> {};

template<bool Cond, class T1, class T2>
struct conditional_type {
    typedef T1 type;
};
template<class T1, class T2>
struct conditional_type<false, T1, T2> {
    typedef T2 type;
};

template<class T>
struct select_nested_const_type {
    typedef typename conditional_type<traits<T>::nest_as_ref, const T&, const T>::type type;
};

template<class T>
struct select_nested_ref_type {
    typedef typename conditional_type<traits<T>::nest_as_ref, T&, T>::type type;
};

struct op_add_tag {};
struct op_sub_tag {};
struct op_mul_tag {};
struct op_div_tag {};
struct op_eq_tag {};
struct op_leq_tag {};
struct op_min_tag {};
struct op_max_tag {};

template<class Tag, class T1, class T2>
struct perform_binary_op;

template<class T1, class T2>
struct perform_binary_op<op_add_tag, T1, T2> {
    inline auto operator()(T1 a, T2 b) { return a + b; }
};

template<class T1, class T2>
struct perform_binary_op<op_sub_tag, T1, T2> {
    inline auto operator()(T1 a, T2 b) { return a - b; }
};

template<class T1, class T2>
struct perform_binary_op<op_mul_tag, T1, T2> {
    inline auto operator()(T1 a, T2 b) { return a * b; }
};

template<class T1, class T2>
struct perform_binary_op<op_div_tag, T1, T2> {
    inline auto operator()(T1 a, T2 b) { return a / b; }
};

template<class T1, class T2>
struct perform_binary_op<op_eq_tag, T1, T2> {
    inline auto operator()(T1 a, T2 b) { return a == b; }
};

template<class T1, class T2>
struct perform_binary_op<op_leq_tag, T1, T2> {
    inline auto operator()(T1 a, T2 b) { return a <= b; }
};

template<class T1, class T2>
struct perform_binary_op<op_min_tag, T1, T2> {
    inline auto operator()(T1 a, T2 b) {
        using namespace std;
        return min(a, b);
    }
};

template<class T1, class T2>
struct perform_binary_op<op_max_tag, T1, T2> {
    inline auto operator()(T1 a, T2 b) {
        using namespace std;
        return max(a, b);
    }
};

template<class Derived>
class ArrayBase;

template<class Tag, class T1, class T2>
class ArrayCwiseOp : public ArrayBase<ArrayCwiseOp<Tag, T1, T2>> {
public:
    ArrayCwiseOp(const ArrayBase<T1> &a, const ArrayBase<T2> &b) : a_(a.derived()), b_(b.derived()) {
        assert(a_.w() == b_.w());
        assert(a_.h() == b_.h());
    }

    decltype(auto) at(int x, int y) const {
        return perform_binary_op<Tag, typename T1::ElementType, typename T2::ElementType>()(a_(x, y), b_(x, y));
    }

    decltype(auto) at(int i) const {
        return perform_binary_op<Tag, typename T1::ElementType, typename T2::ElementType>()(a_(i), b_(i));
    }

    int w() const { return a_.w(); }

    int h() const { return a_.h(); }

private:
    typename select_nested_const_type<T1>::type a_;
    typename select_nested_const_type<T2>::type b_;
};

template<class Tag, class T1, class T2>
struct traits<ArrayCwiseOp<Tag, T1, T2>> {
    typedef typename std::decay_t<T1>::ElementType ET1;
    typedef typename std::decay_t<T2>::ElementType ET2;
    typedef std::invoke_result_t<perform_binary_op<Tag, ET1, ET2>, ET1, ET2> ElementType;

    static const bool nest_as_ref = false;
};

template<class Tag, class T, class Scalar, bool swap = false>
class ArrayScalarOp : public ArrayBase<ArrayScalarOp<Tag, T, Scalar, swap>> {
public:
    ArrayScalarOp(const ArrayBase<T> &a, Scalar b) : a_(a.derived()), b_(b) {}

    decltype(auto) at(int x, int y) const {
        if constexpr (!swap) {
            return perform_binary_op<Tag, typename T::ElementType, Scalar>()(a_(x, y), b_);
        } else {
            return perform_binary_op<Tag, Scalar, typename T::ElementType>()(b_, a_(x, y));
        }
    }

    decltype(auto) at(int i) const {
        if constexpr (!swap) {
            return perform_binary_op<Tag, typename T::ElementType, Scalar>()(a_(i), b_);
        } else {
            return perform_binary_op<Tag, Scalar, typename T::ElementType>()(b_, a_(i));
        }
    }

    inline int w() const { return a_.w(); }

    inline int h() const { return a_.h(); }

private:
    typename select_nested_const_type<T>::type a_;
    const Scalar b_;
};

template<class Tag, class T, class Scalar, bool swap>
struct traits<ArrayScalarOp<Tag, T, Scalar, swap>> {
    typedef typename std::decay_t<T>::ElementType ET;
    typedef std::invoke_result_t<perform_binary_op<Tag, ET, Scalar>, ET, Scalar> ElementType;

    static const bool nest_as_ref = false;
};

template<class T>
class ArrayNegation : public ArrayBase<ArrayNegation<T>> {
public:
    explicit ArrayNegation(const ArrayBase<T> &a) : a_(a.derived()) {}

    bool at(int x, int y) const {
        return !a_(x, y);
    }

    bool at(int i) const {
        return !a_(i);
    }

    int w() const { return a_.w(); }

    int h() const { return a_.h(); }

private:
    typename select_nested_const_type<T>::type a_;
};

template<class T>
struct traits<ArrayNegation<T>> {
    typedef bool ElementType;

    static const bool nest_as_ref = false;
};

template<class T>
class ArraySlice : public ArrayBase<ArraySlice<T>> {
public:
    ArraySlice(ArrayBase<T> &img, int x, int y, int w, int h) : img_(img.derived()), x_(x), y_(y), w_(w), h_(h) {}

    decltype(auto) at(int x, int y) const {
        return img_(x + x_, y + y_);
    }

    auto &at(int x, int y) {
        return img_(x + x_, y + y_);
    }

    decltype(auto) at(int i) const {
        const int y = i / w_;
        const int x = i - y * w_;
        return img_(x + x_, y + y_);
    }

    auto &at(int i) {
        const int y = i / w_;
        const int x = i - y * w_;
        return img_(x + x_, y + y_);
    }

    inline int w() const { return w_; }

    inline int h() const { return h_; }

private:
    typename select_nested_ref_type<T>::type img_;
    const int x_, y_, w_, h_;
};

template<class T>
struct traits<ArraySlice<T>> {
    typedef typename T::ElementType ElementType;

    static const bool nest_as_ref = false;
};

template<class T>
class ArraySliceConst : public ArrayBase<ArraySliceConst<T>> {
public:
    ArraySliceConst(const ArrayBase<T> &img, int x, int y, int w, int h) : img_(img.derived()), x_(x), y_(y), w_(w), h_(h) {}

    decltype(auto) at(int x, int y) const {
        return img_(x + x_, y + y_);
    }

    decltype(auto) at(int i) const {
        const int y = i / w_;
        const int x = i - y * w_;
        return img_(x + x_, y + y_);
    }

    inline int w() const { return w_; }

    inline int h() const { return h_; }

private:
    typename select_nested_const_type<T>::type img_;
    const int x_, y_, w_, h_;
};

template<class T>
struct traits<ArraySliceConst<T>> {
    typedef typename T::ElementType ElementType;

    static const bool nest_as_ref = false;
};

template<class T>
class ArrayChannel : public ArrayBase<ArrayChannel<T>> {
public:
    ArrayChannel(ArrayBase<T> &img, int c) : img_(img.derived()), c_(c) {}

    auto at(int x, int y) const {
        return img_(x, y)(c_);
    }

    auto at(int i) const {
        return img_(i)(c_);
    }

    auto &at(int x, int y) {
        return img_(x, y)(c_);
    }

    auto &at(int i) {
        return img_(i)(c_);
    }

    inline int w() const { return img_.w(); }

    inline int h() const { return img_.h(); }

private:
    typename select_nested_ref_type<T>::type img_;
    const int c_;
};

template<class T>
struct traits<ArrayChannel<T>> {
    typedef typename T::ElementType::ElementType ElementType;

    static const bool nest_as_ref = false;
};

template<class ImgT, class FuncT, class ...Args>
class ArrayTransform : public ArrayBase<ArrayTransform<ImgT, FuncT, Args...>> {
public:
//    typedef const std::function<ElmT(const typename ImgT::ElementType &)> FuncT;

    ArrayTransform(const ArrayBase<ImgT> &img, FuncT fn) : img_(img.derived()), fn_(fn) {}

    auto at(int x, int y) const {
        if constexpr(sizeof...(Args) == 2) return fn_(img_.at(x, y), x, y);
        else return fn_(img_(x, y));
    }

    auto at(int i) const {
        if constexpr(sizeof...(Args) == 2) return fn_(img_.at(i), i);
        else return fn_(img_(i));
    }

    inline int w() const { return img_.w(); }

    inline int h() const { return img_.h(); }

private:
    typename select_nested_const_type<ImgT>::type img_;
    FuncT fn_;
};

template<class ImgT, class FuncT, class ...Args>
struct traits<ArrayTransform<ImgT, FuncT, Args...>> {
    typedef std::invoke_result_t<FuncT, typename ImgT::ElementType, Args...> ElementType;

    static const bool nest_as_ref = false;
};

template<class T>
decltype(auto) sampleBilinear(const T &img, float x, float y) {
    x = clamp(x, 0.f, float(img.w() - 1));
    y = clamp(y, 0.f, float(img.h() - 1));
    int x0 = (int)x;
    int y0 = (int)y;
    int x1 = std::min(x0 + 1, img.w() - 1);
    int y1 = std::min(y0 + 1, img.h() - 1);
    float tx = x - x0;
    float ty = y - y0;
    return lerp(lerp(img(x0, y0), img(x1, y0), tx), lerp(img(x0, y1), img(x1, y1), tx), ty);
}

template<class T>
class ArrayResampler : public ArrayBase<ArrayResampler<T>> {
public:
    ArrayResampler(const T &img, int w, int h) : img_(img.derived()), w_(w), h_(h), mul_x_(1.f / w_ * img_.w()), mul_y_(1.f / h_ * img_.h()), add_x_(.5f * mul_x_ - .5f), add_y_(.5f * mul_y_ - .5f) {}

    inline decltype(auto) at(int x, int y) const {
        return sampleBilinear(img_, x * mul_x_ + add_x_, y * mul_y_ + add_y_);
    }

    inline int w() const { return w_; }

    inline int h() const { return h_; }

private:
    typename select_nested_const_type<T>::type img_;
    const int w_, h_;
    const float mul_x_, mul_y_, add_x_, add_y_;
};

template<class T>
struct traits<ArrayResampler<T>> {
    typedef std::invoke_result_t<decltype(sampleBilinear<T>), const T&, float, float> ElementType;

    static const bool nest_as_ref = false;
};

template<class T>
class Array : public ArrayBase<Array<T>> {
public:
    Array() :Array(0, 0) {}

    Array(int w, int h) : w_(w), h_(h), data_((w > 0 && h > 0) ? new T[w * h] : nullptr) {}

    template<class OtherDerived>
    Array(const ArrayBase<OtherDerived> &other) : Array(other.derived().w(), other.derived().h()) {
        const int w = this->w();
        const int h = this->h();
        for (int y = 0; y < h; ++y) for (int x = 0; x < w; ++x) data_[index(x, y)] = other(x, y);
    }

    Array(const Array &other) : Array(other.derived().w(), other.derived().h())/*:Array(reinterpret_cast<const ArrayBase<Array>&>(other)) */{
        const int w = this->w();
        const int h = this->h();
        for (int y = 0; y < h; ++y) for (int x = 0; x < w; ++x) data_[index(x, y)] = other(x, y);
    }

    Array(Array &&other) noexcept : w_(other.w_), h_(other.h_), data_(other.data_) {
        other.w_ = 0;
        other.h_ = 0;
        other.data_ = nullptr;
    }

    ~Array() {
        delete[] data_;
    }

    inline T at(int x, int y) const {
        // TODO: return by value only if Pixel is primitive
        assert(data_);
        return data_[index(x, y)];
    }

    inline T &at(int x, int y) {
        assert(data_);
        return data_[index(x, y)];
    }

    inline T at(int i) const {
        // TODO: return by value only if Pixel is primitive
        assert(data_);
        return data_[i];
    }

    inline T &at(int i) {
        assert(data_);
        return data_[i];
    }

    Array &operator=(Array other) {
        swap(other);
        return *this;
    }

    inline int w() const { return w_; }

    inline int h() const { return h_; }

    void swap(Array &other) {
        std::swap(w_, other.w_);
        std::swap(h_, other.h_);
        std::swap(data_, other.data_);
    }

    T *begin() { return data_; }
    T *end() { return data_ + this->size(); }
    const T *begin() const { return data_; }
    const T *end() const { return data_ + this->size(); }

    Array gaussianBlur(double sigma) {
        assert(sigma > 0.);

        const int MAX_KERNEL_SIZE = 256;

        float kernel[MAX_KERNEL_SIZE];
        int kernel_size = std::ceil(sigma * 3);

        assert(kernel_size <= MAX_KERNEL_SIZE);

        // precompute the kernel
        double m = 1.f / (sigma * std::sqrt(2 * M_PI));
        for(int i = 0; i <= kernel_size; ++i) {
            double e = (double)i / sigma;
            kernel[i] = (float)(m * std::exp(-0.5 * e * e));
        }

        // horizontally convolved picture
        Array<T> tmp(w_, h_);

        // horizontal convolution
        for (int y = 0; y < h_; ++y) {
            for (int x = 0; x < w_; ++x) {
                T acc = 0;
                int l = std::max(0, x - kernel_size);
                int u = std::min(w_, x + kernel_size + 1);
                for (int xx = l; xx < u; ++xx) {
                    acc += data_[index(xx, y)] * kernel[std::abs(x - xx)];
                }
                tmp(x, y) = acc;
            }
        }

        // fully convolved picture
        Array<T> res(w_, h_);

        // vertical convolution
        for (int y = 0; y < h_; ++y) {
            int l = std::max(0, y - kernel_size);
            int u = std::min(h_, y + kernel_size + 1);
            for (int x = 0; x < w_; ++x) {
                T acc = 0;
                for (int yy = l; yy < u; ++yy) {
                    acc += tmp(x, yy) * kernel[std::abs(y - yy)];
                }
                res(x, y) = acc;
            }
        }

        return res;
    }

private:
    int w_{}, h_{};
    T *data_;

    inline int index(int x, int y) const { return y * w_ + x; }
};

template<class T>
struct traits<Array<T>> {
    typedef T ElementType;

    static const bool nest_as_ref = true;
};

template<class T, int N>
class Vector : public ArrayBase<Vector<T, N>> {
public:
    static const int components = N;

    Vector() = default;

    template<class T1, class T2, class ...Ts>
    Vector(T1 v1, T2 v2, Ts... vs) : data_{T(v1), T(v2), T(vs)...} {} // TODO: initializer_list

    Vector(T v) {
        for(int i = 0; i < N; ++i) data_[i] = v;
    }

    template<class Derived>
    Vector(const ArrayBase<Derived> &other) {
        const Derived &d = other.derived();

        assert(d.w() == 1);
        assert(d.h() >= N);

        for(int i = 0; i < N; ++i) data_[i] = d.at(i);
    }

    template<class Derived>
    Vector &operator=(const ArrayBase<Derived> &other) {
        const Derived &d = other.derived();

        assert(d.w() == 1);
        assert(d.h() >= N);

        for(int i = 0; i < N; ++i) data_[i] = d.at(i);
        return *this;
    }

    template<class Derived>
    Vector &operator+=(const ArrayBase<Derived> &other) {
        const Derived &d = other.derived();

        assert(d.w() == 1);
        assert(d.h() == N);

        for(int i = 0; i < N; ++i) data_[i] += d.at(i);
        return *this;
    }

    template<class Scalar>
    Vector &operator*=(Scalar v) {
        for(int i = 0; i < N; ++i) data_[i] *= v;
        return *this;
    }

    T at(int x, int y) const { return data_[w() * y + x]; }
    T &at(int x, int y) { return data_[w() * y + x]; }
    T at(int i) const { return data_[i]; }
    T &at(int i) { return data_[i]; }

    inline int w() const { return 1; };
    inline int h() const { return N; };

    friend std::ostream &operator<<(std::ostream &os, const Vector &vec) {
        os << '(' << vec(0);
        for(int i = 1; i < vec.size(); ++i) os << ", " << vec(i);
        os << ')';
        return os;
    }
private:
    T data_[N];
};

template<class T, int N>
struct traits<Vector<T, N>> {
    typedef T ElementType;

    static const bool nest_as_ref = false;
};

#define ARRAY_OPERATOR(OP,TAG) \
    template<class OtherDerived>\
    inline ArrayCwiseOp<TAG, Derived, OtherDerived> operator OP (const ArrayBase<OtherDerived> &other) const { return {derived(), other.derived()}; }\
    template<class Scalar, std::enable_if_t<std::is_arithmetic_v<Scalar>, int> = 0>\
    inline ArrayScalarOp<TAG, Derived, Scalar> operator OP (Scalar s) const { return {derived(), s}; }\
    template<class Scalar, std::enable_if_t<std::is_arithmetic_v<Scalar>, int> = 0>\
    friend inline ArrayScalarOp<TAG, Derived, Scalar, true> operator OP (Scalar s, const ArrayBase<Derived> &img) { return {img.derived(), s}; }

template<class Derived>
class ArrayBase {
public:
    typedef typename traits<Derived>::ElementType ElementType;

    inline const Derived &derived() const { return *static_cast<const Derived *>(this); }

    inline Derived &derived() { return *static_cast<Derived *>(this); }

    inline ElementType operator()(int x, int y) const {
        assert(x >= 0);
        assert(x < w());
        assert(y >= 0);
        assert(y < h());
        return derived().at(x, y);
    }

    inline ElementType &operator()(int x, int y) {
        assert(x >= 0);
        assert(x < w());
        assert(y >= 0);
        assert(y < h());
        return derived().at(x, y);
    }

    inline const ElementType operator()(int i) const {
        assert(i >= 0);
        assert(i < size());
        return derived().at(i);
    }

    inline ElementType &operator()(int i) {
        assert(i >= 0);
        assert(i < size());
        return derived().at(i);
    }

    inline ElementType operator()(const Vector<int, 2> i) const {
        return (*this)(i(0), i(1));
    }

    inline ElementType &operator() (const Vector<int, 2> i) {
        return (*this)(i(0), i(1));
    }

    inline int w() const { return derived().w(); }

    inline int h() const { return derived().h(); }

    inline int size() const { return w() * h(); }

    ARRAY_OPERATOR(+,op_add_tag)
    ARRAY_OPERATOR(-,op_sub_tag)
    ARRAY_OPERATOR(*,op_mul_tag)
    ARRAY_OPERATOR(/,op_div_tag)
    ARRAY_OPERATOR(==,op_eq_tag)
    ARRAY_OPERATOR(<=,op_leq_tag)

    inline ArrayNegation<Derived> operator!() const {
        return {derived()};
    }

    template<class FuncT>
    inline auto transform(const FuncT &fn) {
        return ArrayTransform(derived(), fn);
    }

    inline ArraySlice<Derived> slice(int x, int y, int w, int h) {
        return {derived(), x, y, w, h};
    }

    inline ArraySliceConst<Derived> slice(int x, int y, int w, int h) const {
        return {derived(), x, y, w, h};
    }

    inline ArrayResampler<Derived> resize(int w, int h) {
        return {derived(), w, h};
    }

    inline ArrayChannel<Derived> channel(int c) {
        return {derived(), c};
    }

    template<class T = ElementType>
    T sum() const {
        T res = 0;
        const Derived &d = derived();
        const int s = d.size();
        for(int i = 0; i < s; ++i) res += d(i);
        return res;
    }
};

template<class T>
inline decltype(auto) sampleBilinear(const T &img, const Vector<float, 2> &i) {
    return sampleBilinear(img, i(0), i(1));
}

#define ARRAY_FUNCTION(FN,TAG) \
    template<class T1, class T2>\
    inline ArrayCwiseOp<TAG, T1, T2> FN (const ArrayBase<T1> &a, const ArrayBase<T2> &b) { return {a.derived(), b.derived()}; }\
    template<class T, class Scalar, std::enable_if_t<std::is_arithmetic_v<Scalar>, int> = 0>\
    inline ArrayScalarOp<TAG, T, Scalar> FN (const ArrayBase<T> &img, Scalar s) { return {img.derived(), s}; }\
    template<class T, class Scalar, std::enable_if_t<std::is_arithmetic_v<Scalar>, int> = 0>\
    inline ArrayScalarOp<TAG, T, Scalar, true> FN (Scalar s, const ArrayBase<T> &img) { return {img.derived(), s}; }

ARRAY_FUNCTION(min, op_min_tag)
ARRAY_FUNCTION(max, op_max_tag)

template<int N>
struct select_read_element_type {
    typedef Vector<float, N> type;
};

template<>
struct select_read_element_type<1> {
    typedef float type;
};

template<int N, class ... Args>
auto readImage(const char *format, Args ... args) {
    char filename[255];
    sprintf(filename, format, args...);

    int w, h, n;

    static_assert(N >= 1 && N <= 4);

    unsigned char *data = stbi_load(filename, &w, &h, &n, N);

    if (!data) throw std::runtime_error(std::string("File not found ") + filename);

    Array<typename select_read_element_type<N>::type> img(w, h);

    // TODO: rewrite
    for (int y = 0; y < h; ++y) {
        for (int x = 0; x < w; ++x) {
            if constexpr (N == 1) {
                img.at(x, y) = data[w * y + x] / 255.;
            } else {
                for (int c = 0; c < N; ++c) {
                    img.at(x, y)(c) = data[(w * y + x) * N + c] / 255.;
                }
            }
        }
    }

    stbi_image_free(data);

    return img;
}

template<class T, class ... Args>
void writeImage(const ArrayBase<T> &img, const char *format, Args ... args) {
    char filename[255];
    sprintf(filename, format, args...);

    using ImgT = std::decay_t<decltype(img)>;
    const bool single_channel = std::is_arithmetic_v<std::remove_reference_t<decltype(img(0, 0))>>;

    const int w = img.w();
    const int h = img.h();

    int n = 1;
    int multiple = 1;

    if constexpr (single_channel) {
        using ElmT = typename ImgT::ElementType;
        if constexpr (std::is_floating_point_v<ElmT> || std::is_same_v<ElmT, bool>) multiple = 255;
    } else {
        using ElmT = typename ImgT::ElementType::ElementType;
        if constexpr (std::is_floating_point_v<ElmT> || std::is_same_v<ElmT, bool>) multiple = 255;
        n = img(0, 0).size();
    }

    auto *data = new unsigned char[w * h * n];

    // TODO: rewrite
    for(int y = 0; y < h; ++y) {
        for (int x = 0; x < w; ++x) {
            if constexpr (single_channel) data[w * y + x] = img(x, y) * multiple;
            else {
                for (int c = 0; c < n; ++c) {
                    data[(w * y + x) * n + c] = img(x, y)(c) * multiple;
                }
            }
        }
    }

    stbi_write_png(filename, w, h, n, data, 0);

    // TODO: check return value

    delete[] data;
}

#endif //LAZYFLUIDS_POKUSY_ARRAY_H
