# lazyfluids pokusy

## build

Na linuxu s CMake

```
mkdir -p build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Release
make
```

vytvoří v adresáři build spustitelný soubor lazyfluids.

## spuštění

Je to zatím jenom demo, které očekává v pracovním adresáři podadresáře source, target-mask a target-flow
se soubory #####.png/A2V2f.
Vystupuje do adresáře output v pracovním adresáři, který musí předem existovat.
