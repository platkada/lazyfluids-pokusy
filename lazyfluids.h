#ifndef LAZYFLUIDS_POKUSY_LAZYFLUIDS_H
#define LAZYFLUIDS_POKUSY_LAZYFLUIDS_H

#include <vector>
#include <algorithm>
#include "patchmatch.h"
#include "typedefs.h"

struct OffsetError {
    OffsetError(const Vector<int, 2> &so, const Vector<int, 2> &to, float e) : source_offset (so), target_offset(to), error(e) {}

    Vector<int, 2> source_offset;
    Vector<int, 2> target_offset;
    float error;
};

template<class MaskT>
void lazyfluids_init_offsets(Arr2i &offsets, const MaskT &target_mask, const Channels &source, const Channels &target, int patch_size) {
    const int padding = patch_size / 2;
    std::vector<Vec2i> legal_offsets[3]; // TODO: first one always empty, rewrite

    for(int y = 0; y < target_mask.h(); ++y) {
        for(int x = 0; x < target_mask.w(); ++x) {
            if(target_mask(x, y)) {
                const unsigned char target_seg = target.segment(x + padding, y + padding);
                legal_offsets[target_seg].emplace_back(x + padding, y + padding);
            }
        }
    }

    for(int y = 0; y < offsets.h(); ++y) {
        for(int x = 0; x < offsets.w(); ++x) {
            const unsigned char source_seg = source.segment(x + padding, y + padding);
            if(legal_offsets[source_seg].empty()) continue; // don't care about an empty segment
            offsets(x, y) = legal_offsets[source_seg][fastrand() % legal_offsets[source_seg].size()];
        }
    }
}

void lazyfluids_compute_nnf(const Channels &source, const Channels &target, Arr2i &offsets, const int patch_size) {
    const int source_w = source.rgba.w();
    const int source_h = source.rgba.h();
    const int target_w = target.rgba.w();
    const int target_h = target.rgba.h();

    const int padding = patch_size / 2;

    Arr2i offsets_inv(source_w - patch_size + 1, source_h - patch_size + 1);

    Arrf errors(source_w - patch_size + 1, source_h - patch_size + 1);
    std::fill(errors.begin(), errors.end(), 0);

    Arri source_usages(source_w - patch_size + 1, source_h - patch_size + 1);
    std::fill(source_usages.begin(), source_usages.end(), 0);

    const auto target_patch_segments = target.segment.slice(padding, padding, target_w - patch_size + 1, target_h - patch_size + 1);
    const auto source_patch_segments = source.segment.slice(padding, padding, source_w - patch_size + 1, source_h - patch_size + 1);

    const int target_size_b = (target_patch_segments == 1).sum<int>();
    const int target_size_i = (target_patch_segments == 2).sum<int>();
    const int source_size_b = (source_patch_segments == 1).sum<int>();
    const int source_size_i = (source_patch_segments == 2).sum<int>();

    const int K_i = target_size_i / source_size_i;
    int R_i = target_size_i % source_size_i;
    const int K_b = target_size_b / source_size_b;
    int R_b = target_size_b % source_size_b;

    Arrb target_mask = target_patch_segments;

    int target_coverage = target_size_i + target_size_b;

    while(target_coverage > 0) {
        lazyfluids_init_offsets(offsets_inv, target_mask, source, target, patch_size);

        patchmatch(source, target, target_mask, offsets_inv, errors, patch_size); // TODO: jinak kdyz je maska ridka

        std::vector<OffsetError> mapping;
        for(int y = 0; y < source_h - patch_size + 1; ++y) {
            for(int x = 0; x < source_w - patch_size + 1; ++x) {
                if(!source_patch_segments(x, y)) continue;
                mapping.emplace_back(Vec2i(x, y) + padding, offsets_inv(x, y), errors(x, y));
            }
        }

        std::sort(mapping.begin(), mapping.end(), [](const OffsetError &a, const OffsetError &b){ return a.error < b.error; });

        for(const auto &offset_error : mapping) {
            const auto source_offset = offset_error.source_offset;
            const auto target_offset = offset_error.target_offset;

            bool &target_free = target_mask(target_offset - padding);

            if(!target_free) continue; // skip duplicates

            const unsigned char source_seg = source.segment(source_offset);

            assert(source_seg);
            assert(source_seg == target.segment(target_offset));

            const int K = source_seg == 2 ? K_i : K_b;
            int &R = source_seg == 2 ? R_i : R_b;

            int &source_usage = source_usages(source_offset - padding);

            if(source_usage < K) {
                ++source_usage;
                target_free = false;
                offsets(target_offset - padding) = source_offset;
                --target_coverage;
            } else if((source_usage == K) && (R > 0)) {
                ++source_usage;
                target_free = false;
                offsets(target_offset - padding) = source_offset;
                --target_coverage;
                --R;
            }
        }
    }
}

#endif //LAZYFLUIDS_POKUSY_LAZYFLUIDS_H
