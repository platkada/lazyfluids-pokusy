#include <iostream>
#include <ctime>
#include "Array.h"
#include "patchmatch.h"
#include "lazyfluids.h"
#include "util.h"
#include "typedefs.h"
#include "read_image_theirs.h"

int main() {
    PROFILE(main);

    const int patch_size = 5;
    const int frames = 211;

    Arr4f target_rgba_prev;

    for(int i = 0; i < frames; ++i) {
        // TODO: keep already read files

        std::cout << "frame " << i << std::endl;

        Channels source(readImage<4>("source/%05d.png", i), readImage<4>("source/%05d.png", std::max(i - 1, 0)));

        auto target_a = readImage<1>("target-mask/%05d.png", i);
        auto target_rgba = target_a.transform([](float a){ return Vec4f(0.f, 0.f, 0.f, a); });

        auto flow = readImageTheirs<2>("target-flow/%05d.A2V2f", i);
        Arr4f target_rgba_prev_warped(target_rgba.w(), target_rgba.h());

        if(i) {
            for(int y = 0; y < target_rgba_prev_warped.h(); ++y) for(int x = 0; x < target_rgba_prev_warped.w(); ++x) {
                target_rgba_prev_warped(x, y) = sampleBilinear(target_rgba_prev, Vec2f(x, y) - flow(x, y));
            }
        } else {
            for(int y = 0; y < target_rgba_prev_warped.h(); ++y) for(int x = 0; x < target_rgba_prev_warped.w(); ++x) {
                target_rgba_prev_warped(x, y) = sampleBilinear(target_rgba, Vec2f(x, y) - flow(x, y));
            }
        }

        Channels target(target_rgba, target_rgba_prev_warped);

        const int target_w = target.rgba.w();
        const int target_h = target.rgba.h();

        Arr2i offsets(target_w - patch_size + 1, target_h - patch_size + 1);

        // TODO: trim images

        Arrf temporal_modulation(target_w, target_h);

        if(i) {
            // TODO: param lambda
            temporal_modulation = 0.2f * (1.f - max(target_rgba.channel(3) - target_rgba_prev_warped.channel(3), 0.f));
        } else {
            std::fill(temporal_modulation.begin(), temporal_modulation.end(), 0.f);
        }

        target.temporal_modulation = std::move(temporal_modulation);

        lazyfluids_compute_nnf(source, target, offsets, patch_size);

        patchmatch_vote(target.rgba, target.segment, source.rgba, source.segment, offsets, patch_size);
        writeImage(target.rgba, "output/%05d.png", i);

        target_rgba_prev = std::move(target.rgba);
    }

    PROFILE_END(main)

    return 0;
}