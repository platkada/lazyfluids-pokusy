#ifndef LAZYFLUIDS_POKUSY_PATCHMATCH_H
#define LAZYFLUIDS_POKUSY_PATCHMATCH_H

#include <vector>
#include <random>
#include "typedefs.h"

void patchmatch_vote(Arr4f &dst, const Arrb &dst_mask, const Arr4f &src, const Arrb &src_mask, const Arr2i &offsets, int patch_size) {
    const int dst_w = dst.w();
    const int dst_h = dst.h();

    const int padding = patch_size / 2;

    for(int y = 0; y < dst_h; ++y) {
        for (int x = 0; x < dst_w; ++x) {

            const Vec2i xy{x, y};
            Vec4f sum = 0;

            const int px_l = std::max(-padding, padding - x);
            const int px_u = std::min(padding, dst_w - padding - x - 1);
            const int py_l = std::max(-padding, padding - y);
            const int py_u = std::min(padding, dst_h - padding - y - 1);

            int area = 0;//(px_u - px_l + 1) * (py_u - py_l + 1);

            for(int py = py_l; py <= py_u; ++py) {
                for(int px = px_l; px <= px_u; ++px) {
                    Vec2i pxy{px, py};
                    if(!dst_mask(xy + pxy)) continue;
                    Vec2i src_offset = offsets(xy + pxy - padding);
                    if(!src_mask(src_offset)) continue;
                    sum += src(src_offset - pxy);
                    ++area;
                }
            }

            if(area) {
                dst(x, y) = sum / area;
            } else {
                dst(x, y) = sum;
            }
        }
    }
}

// TODO: general
inline float patch_dist(const Channels &src, const Vec2i src_offset, const Channels &dst, const Vec2i dst_offset, int patch_size, float best = 10000) {
    float sum = 0;

    const int padding = patch_size / 2;

    const int px_l = -padding;
    const int px_u = padding;
    const int py_l = -padding;
    const int py_u = padding;

    for(int py = py_l; py <= py_u; ++py) {
        for(int px = px_l; px <= px_u; ++px) {
            const Vec2i pxy{px, py};

            const auto curr_diff = src.rgba(src_offset + pxy) - dst.rgba(dst_offset + pxy);
            const auto curr_diff_sqr = curr_diff * curr_diff;
            sum += curr_diff_sqr(0) + curr_diff_sqr(1) + curr_diff_sqr(2) + 3 * curr_diff_sqr(3);

            const auto prev_diff = src.rgba_prev(src_offset + pxy) - dst.rgba_prev(dst_offset + pxy);
            const auto prev_diff_sqr = prev_diff * prev_diff;
            sum += (prev_diff_sqr(0) + prev_diff_sqr(1) + prev_diff_sqr(2) + 3 * prev_diff_sqr(3)) * src.temporal_modulation(src_offset + pxy);

            const auto blur_diff = src.a_blur(src_offset + pxy) - dst.a_blur(dst_offset + pxy);
            const auto blur_diff_sqr = blur_diff * blur_diff;
            sum += 3.f * blur_diff_sqr; // TODO: param eta

            if(sum > best) goto end;
        }
    }

    end:
    return sum;

}

void patchmatch(const Channels &dst, const Channels &src, const Arrb &src_mask, Arr2i &offsets, Array<float> &errors, int patch_size) {
    const int dst_w = dst.rgba.w();
    const int dst_h = dst.rgba.h();
    const int src_w = src.rgba.w();
    const int src_h = src.rgba.h();

    assert(patch_size <= dst_w);
    assert(patch_size <= dst_h);

    const int padding = patch_size / 2;

    const int iterations = 4;

    for(int i = 0; i < iterations; ++i) {
        int dir;
        int begin_x, begin_y, end_x, end_y;

        if(i % 2) {
            dir = -1;
            end_x = end_y = padding -1;
            begin_x = dst_w - padding - 2;
            begin_y = dst_h - padding - 2;
        } else {
            dir = 1;
            begin_x = begin_y = padding + 1;
            end_x = dst_w - padding;
            end_y = dst_h - padding;
        }

        for (int y = begin_y; y != end_y; y += dir) {
            for (int x = begin_x; x != end_x; x += dir) {
                const Vec2i dst_xy(x, y);
                const unsigned char dst_seg = dst.segment(dst_xy);
                if (!dst_seg) continue; // TODO: general
                auto &src_xy = offsets(dst_xy - padding);
                float dist = i ? errors(dst_xy - padding) : patch_dist(src, src_xy, dst, dst_xy, patch_size);

                // propagation
                if(dst.segment(dst_xy - Vec2i(dir, 0))) { // TODO: general
                    Vec2i src_xy2 = offsets(dst_xy - padding - Vec2i(dir, 0));
                    src_xy2(0) += dir;

                    if((dir == 1 ? src_xy2(0) < src_w - padding : src_xy2(0) >= padding) && src.segment(src_xy2) == dst_seg && src_mask(src_xy2 - padding)) {
                        const float dist2 = patch_dist(src, src_xy2, dst, dst_xy, patch_size, dist);

                        if (dist2 < dist) {
                            src_xy = src_xy2;
                            dist = dist2;
                        }
                    }
                }
                if(dst.segment(dst_xy - Vec2i(0, dir))) { // TODO: general
                    Vec2i src_xy2 = offsets(dst_xy - padding - Vec2i(0, dir));
                    src_xy2(1) += dir;

                    if ((dir == 1 ? src_xy2(1) < src_h - padding : src_xy2(1) >= padding) && src.segment(src_xy2) == dst_seg && src_mask(src_xy2 - padding)) {
                        const float dist2 = patch_dist(src, src_xy2, dst, dst_xy, patch_size, dist);

                        if (dist2 < dist) {
                            src_xy = src_xy2;
                            dist = dist2;
                        }
                    }
                }

                // random search
                const int radius_max = std::max(std::max(src_xy(0), src_w - src_xy(0)), std::max(src_xy(1), src_h - src_xy(1)));
                const float alpha = 0.5;
                float coeff = 1;
                int radius;
                Vec2i search_base = src_xy;
                while((radius = coeff * radius_max) > 1) {
                    const int min_x = std::max(padding, search_base(0) - radius);
                    const int max_x = std::min(src_w - padding, search_base(0) + radius);
                    const int min_y = std::max(padding, search_base(1) - radius);
                    const int max_y = std::min(src_h - padding, search_base(1) + radius);

                    const Vec2i src_xy2{min_x + fastrand() % (max_x - min_x), min_y + fastrand() % (max_y - min_y)};

                    if(src.segment(src_xy2) == dst_seg && src_mask(src_xy2 - padding)) {
                        float dist2 = patch_dist(src, src_xy2, dst, dst_xy, patch_size, dist);

                        if (dist2 < dist) {
                            src_xy = src_xy2;
                            dist = dist2;
                        }
                    }

                    coeff *= alpha;
                }

                errors(dst_xy - padding) = dist;
            }
        }
    }
}

#endif //LAZYFLUIDS_POKUSY_PATCHMATCH_H
