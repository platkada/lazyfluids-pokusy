#ifndef LAZYFLUIDS_POKUSY_READ_IMAGE_THEIRS_H
#define LAZYFLUIDS_POKUSY_READ_IMAGE_THEIRS_H

#include "jzq.h"

// TODO: remove

template<typename T>
bool a2read(Array2<T>* out_A,const std::string& fileName)
{
    FILE* f = fopen(fileName.c_str(),"rb");

    if(!f) { return false; }

    int w,h;

    if(fread(&w,sizeof(w),1,f)!=1 ||
       fread(&h,sizeof(h),1,f)!=1 ||
       ((w*h)<1))
    {
        fclose(f);
        return false;
    }

    Array2<T> A(w,h);

    if(fread(A.data(),sizeof(T)*w*h,1,f)!=1)
    {
        fclose(f);
        return false;
    }

    if(out_A!=0) { *out_A = A; }

    fclose(f);
    return true;
}

template<typename T>
Array2<T> a2read(const std::string& fileName)
{
    Array2<T> A;
    if(!a2read(&A,fileName)) { throw std::runtime_error(std::string("File not found ") + fileName); }
    return A;
}

template<int N, class ... Args>
auto readImageTheirs(const char *format, Args ... args) {
    char filename[255];
    sprintf(filename, format, args...);

    static_assert(N >= 1 && N <= 4);

    A2V2f img_theirs = a2read<Vec<N, float>>(filename);

    Array<Vector<float, N>> img(img_theirs.width(), img_theirs.height());

    for(int y=0;y<img_theirs.height();y++) for(int x=0;x<img_theirs.width();x++) {
        for(int c = 0; c < N; ++c) img(x, y)(c) = img_theirs(x, y)(c);
    }

    return img;
}

#endif //LAZYFLUIDS_POKUSY_READ_IMAGE_THEIRS_H
