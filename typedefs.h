#ifndef LAZYFLUIDS_POKUSY_TYPEDEFS_H
#define LAZYFLUIDS_POKUSY_TYPEDEFS_H

typedef Vector<int, 2> Vec2i;
typedef Vector<float, 2> Vec2f;
typedef Vector<float, 3> Vec3f;
typedef Vector<float, 4> Vec4f;

typedef Array<float> Arrf;
typedef Array<int> Arri;
typedef Array<bool > Arrb;

typedef Array<Vec2i> Arr2i;

typedef Array<Vec3f> Arr3f;
typedef Array<Vec4f> Arr4f;

// TODO: move someplace else
struct Channels {
    template<class rgbaT, class rgba_prevT>
    Channels(rgbaT &&_rgba, rgba_prevT &&_rgba_prev)
            :rgba(std::forward<rgbaT>(_rgba)),
             a_blur(Arrf(rgba.channel(3)).gaussianBlur(1.5f)), // TODO: parameter sigma
             rgba_prev(std::forward<rgba_prevT>(_rgba_prev)),
             segment(a_blur.transform([](float v) -> unsigned char { return (v < .1f) ? 0 : ((v < .9f) ? 1 : 2); })), // TODO: param l, u
             temporal_modulation()
    {}

    Arr4f rgba;
    Arrf a_blur;
    Arr4f rgba_prev;
    Array<unsigned char> segment;
    Arrf temporal_modulation;
};

#endif //LAZYFLUIDS_POKUSY_TYPEDEFS_H
