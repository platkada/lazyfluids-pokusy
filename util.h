#ifndef LAZYFLUIDS_POKUSY_UTIL_H
#define LAZYFLUIDS_POKUSY_UTIL_H

#define PROFILE(name) clock_t _ ## name ## _start = std::clock();
#define PROFILE_END(name) clock_t _ ## name ## _end = std::clock(); std::cout << "Profile " << #name << ": " << (double(_ ## name ## _end - _ ## name ## _start) / CLOCKS_PER_SEC) << std::endl;

template<class T>
T clamp(T v, T min, T max) {
    return std::max(min, std::min(max, v));
}

template<class T>
decltype(auto) lerp(const T &a, const T &b, float t) {
    return a * (1.f - t) + b * t;
}

static unsigned int g_seed = 0;
inline int fastrand() {
    g_seed = (214013*g_seed+2531011);
    return (g_seed>>16)&0x7FFF;
}

#endif //LAZYFLUIDS_POKUSY_UTIL_H
